# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:pollakmartin/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/pollakmartin/stroboskop/commits/c8b664d8b632c95264c6208bf795a6edc24652f2

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/pollakmartin/stroboskop/commits/ae1f3edc93cd54de35c8c90a1ab81cd92dfb10b2

Naloga 6.3.2:
https://bitbucket.org/pollakmartin/stroboskop/commits/05ef32c5ec7a35a0cf9144521720d581ad446d2a

Naloga 6.3.3:
https://bitbucket.org/pollakmartin/stroboskop/commits/08a5fad8bf753273c777248e61e063c11dcc5e47

Naloga 6.3.4:
https://bitbucket.org/pollakmartin/stroboskop/commits/5b5b04876c6ec7ed79083d8b8873be6af125abe9

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/pollakmartin/stroboskop/commits/694fa67d46c354203432650d8574a3ca4a12888f

Naloga 6.4.2:
https://bitbucket.org/pollakmartin/stroboskop/commits/4b55c8f8b1720396f0680ac4b4e05baabfe75052

Naloga 6.4.3:
https://bitbucket.org/pollakmartin/stroboskop/commits/8b57e27823a4469ae6eda5c7b3dd746f8daa99a5

Naloga 6.4.4:
https://bitbucket.org/pollakmartin/stroboskop/commits/ce66672f6eba5f356de5008da8d1bd405bf70be9